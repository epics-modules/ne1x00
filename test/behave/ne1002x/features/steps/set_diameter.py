from behave import given, when, then

@given('user enters {diameter} value')
def step_given_entered_value(context, diameter):
    pass

@when('user presses Enter key')
def step_when_press_enter(context):
    assert True is not False

@then('entered value should {set_result} set into device')
def step_then_verify_set_result(context, set_result):
    assert context.failed is False

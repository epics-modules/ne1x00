# -*- coding: utf-8 -*-
# *********************************************************************
# lewis - a library for creating hardware device simulators
# Copyright (C) 2016-2017 European Spallation Source ERIC
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# *********************************************************************

from lewis.adapters.stream import StreamInterface, Cmd
from lewis.core import approaches
from lewis.core.statemachine import State
from lewis.devices import Device, StateMachineDevice

from curses.ascii import STX, ETX, CR, LF
# -----------------------------------------------
# 0D : CR  : '\r' (Carriage Return)
# 0A : LF  : '\n' (Line Feed)
# -----------------------------------------------
# 02 : STX : ???? (Start of Text)
# 03 : ETX : ???? (End of Text)

from enum import Enum
from collections import OrderedDict


class NE1002xOperations(Enum):
    idle            = 0
    withdrawing     = 1
    infusing        = 2
    purging         = 3
    prog_paused     = 4
    prog_stopped    = 5

    # overriding default Enum str() casting to return its 'name'
    def __str__(self):
        return self.name

    # overriding default Enum str() casting to return its 'value'
    def __int__(self):
        return self.value


class NE1002xDirections(Enum):
    infuse      = 'INF'
    withdraw    = 'WDR'
    reverse     = 'REV'
    sticky      = 'STK'

    # overriding default Enum str() casting to return its 'value'
    def __str__(self):
        return self.value


class NE1002xRateUnits(Enum):
    # UL/MIN; ML/MIN; UL/HOUR; ML/HOUR
    um_per_minute   = 'UM'
    mm_per_minute   = 'MM'
    um_per_hour     = 'UH'
    mm_per_hour     = 'MH'

    # overriding default Enum str() casting to return its 'value'
    def __str__(self):
        return self.value


class NE1002xVolumeUnits(Enum):
    # UL; ML
    microliter  = 'UL'
    milliliter  = 'ML'

    # overriding default Enum str() casting to return its 'value'
    def __str__(self):
        return self.value


class NE1002xMovingState(State):
    def on_entry(self, dt):
        #print("NE1002xMovingState - on_entry")
        # set the internal speed according to selected operation
        if self._context._operation == NE1002xOperations.purging:
            self._context._speed_selected = self._context._max_speed
        else:
            self._context._speed_selected = self._context.speed

    def in_state(self, dt):
        #print("NE1002xMovingState - in_state")
        initial_volume = self._context._volume_dispensed
        # updating dispensed volume
        self._context._volume_dispensed = approaches.linear(initial_volume,
            self._context._volume,
            self._context._speed_selected,
            dt)
        # calculate increased volume in this cycle
        delta_volume = self._context._volume_dispensed - initial_volume
        # updating operational volume according to direction (using delta)
        if self._context._direction == NE1002xDirections.withdraw:
            self._context._volume_dis_withdrawn += delta_volume
        elif self._context._direction == NE1002xDirections.infuse:
            self._context._volume_dis_infused += delta_volume
        # logging
        self.log.info('Volume pumped (%s -> %s), volume=%s, speed=%s', 
            initial_volume,
            self._context._volume_dispensed, 
            self._context._volume, 
            self._context._speed_selected)
        # verify if reached target (then stop)
        if self._context._volume_dispensed >= self._context._volume:
            self._context._operation = NE1002xOperations.prog_stopped

    def on_exit(self, dt):
        #print("NE1002xMovingState - on_exit")
        pass


class NE1002xPausedState(State):
    def on_entry(self, dt):
        #print("NE1002xPausedState - on_entry")
        pass

    def in_state(self, dt):
        #print("NE1002xPausedState - in_state")
        pass

    def on_exit(self, dt):
        #print("NE1002xPausedState - on_exit")
        pass


class NE1002xStoppedState(State):
    def on_entry(self, dt):
        #print("NE1002xStoppedState - on_entry")
        # reset the internal volume on this cycle
        self._context._volume_dispensed = 0.0

    def in_state(self, dt):
        #print("NE1002xStoppedState - in_state")
        pass

    def on_exit(self, dt):
        #print("NE1002xStoppedState - on_exit")
        pass


class NE1002xSimulated(StateMachineDevice):
    def _initialize_data(self):
        # ---------------------------------------------------------------------
        # NE 1002x
        # ---------------------------------------------------------------------
        # internal
        self._operation = NE1002xOperations.idle
        self._volume_dispensed  = 0.0
        self._speed_selected    = 0.0
        self._max_speed = 5.1005   # cm/min
        self._min_speed = 0.004205 # cm/hr
        self._max_rate  = 1699 # mL/hr
        self._min_rate  = 0.73 # uL/hr
        self._min_volume = 0.0 # uL
        self._max_volume = 999.9 # mL
        self._max_inside_diameter = 50.00 # mm
        self._min_inside_diameter = 0.100 # mm
        # ---------------------------------------------------------------------
        # static
        self._firmware_version = "NE1002xSIMv0.01"
        self._address = "00"
        # ---------------------------------------------------------------------
        self.position   = 0.0
        self._target    = 0.0
        self.speed      = self._max_speed
        # ---------------------------------------------------------------------
        # operational
        self._diameter  = self._min_inside_diameter
        self._volume        = 0.0
        self._volume_unit   = NE1002xVolumeUnits.microliter
        self._rate      = self._min_rate
        self._rate_unit = NE1002xRateUnits.um_per_minute
        self._direction = NE1002xDirections.withdraw
        self._volume_dis_infused    = 0.0
        self._volume_dis_withdrawn  = 0.0
        self._volume_dis_unit       = NE1002xVolumeUnits.microliter

    def _get_state_handlers(self):
        return {
            'idle':     State(),
            'moving':   NE1002xMovingState(),
            'paused':   NE1002xPausedState(),
            'stopped':  NE1002xStoppedState()
        }

    def _get_initial_state(self):
        return 'idle'

    def _get_transition_handlers(self):
        return OrderedDict([
            (('idle', 'moving'), lambda: self._operation in (NE1002xOperations.infusing, NE1002xOperations.withdrawing, NE1002xOperations.purging)),
            (('moving', 'paused'), lambda: self._operation == NE1002xOperations.prog_paused),
            (('moving', 'stopped'), lambda: self._operation == NE1002xOperations.prog_stopped),
            (('paused', 'moving'), lambda: self._operation in (NE1002xOperations.infusing, NE1002xOperations.withdrawing, NE1002xOperations.purging)),
            (('paused', 'stopped'), lambda: self._operation == NE1002xOperations.prog_stopped),
            (('stopped', 'moving'), lambda: self._operation in (NE1002xOperations.infusing, NE1002xOperations.withdrawing, NE1002xOperations.purging)),
            ])

    def _get_return_data(self, data):
        # ---------------------------------------------------------------------
        # returning with the format:
        #   <START OF TEXT terminator>,<DEVICE ADDRESS>,<DEVICE STATUS>,<DATA>[,..,<DATA-n>,<DATA_UNIT>]
        # ---------------------------------------------------------------------
        return_data = "%s%s%s" % (chr(STX), self._address, self.status)
        for item in data:
            return_data += str(item)
        return return_data

    # -------------------------------------------------------------------------
    # PROPERTIES
    # -------------------------------------------------------------------------
    # state
    @property
    def state(self):
        return self._csm.state

    # -------------------------------------------------------------------------
    # status
    @property
    def status(self):
        # ---------------------------------------------------------------------
        # Status - enum [ I | W | S | P | T | U | X ]
        #    >  I = Infusing
        #    >  W = Withdrawing
        #    >  S = Pumping Program Stopped
        #    >  P = Pumping Program Paused
        #    >  T = Pause Phase
        #    >  U = Operational trigger wait (user wait)
        #    >  X = Purging
        # ---------------------------------------------------------------------
        # translate from internal Enum to device answers
        if self._operation == NE1002xOperations.infusing:
            return "I"
        elif self._operation == NE1002xOperations.withdrawing:
            return "W"
        elif self._operation == NE1002xOperations.prog_stopped:
            return "S"
        elif self._operation == NE1002xOperations.prog_paused:
            return "P"
        elif self._operation == NE1002xOperations.purging:
            return "X"
        else:
            return "S"

    # -------------------------------------------------------------------------
    # firmware version
    @property
    def firmware_version(self):
        return self._get_return_data([self._firmware_version])

    # -------------------------------------------------------------------------
    # direction
    @property
    def direction(self):
        return self._get_return_data([self._direction])

    @direction.setter
    def direction(self, new_direction):
        if self.state == 'moving':
            # force it to pause .. then stop
            self._operation = NE1002xOperations.prog_paused
            self._operation = NE1002xOperations.prog_stopped
        # ---------------------------------------------------------------------
        # set new direction using Enum
        if 'REV' in new_direction:
            if self._direction == NE1002xDirections.infuse:
                self._direction = NE1002xDirections.withdraw
            else:
                self._direction = NE1002xDirections.infuse
        else:
            dirIsOK = False
            for dirItem in NE1002xDirections:
                if str(dirItem) in new_direction:
                    self._direction = dirItem
                    dirIsOK = True
                    break
            if not dirIsOK:
               raise ValueError('Direction unknown')

    # -------------------------------------------------------------------------
    # rate
    @property
    def rate(self):
        return self._get_return_data([self._rate, self._rate_unit])

    @rate.setter
    def rate(self, new_rate):
        for param in new_rate:
            if type(param) == float:
                if not (self._min_rate <= param <= self._max_rate):
                   raise ValueError('Rate out of range [%f, %f]' % (self._min_rate, self._max_rate))
                # set rate (float)
                self._rate = param
            elif type(param) == str:
                rateUnitIsOK = False
                for rateUnit in NE1002xRateUnits:
                    if str(rateUnit) in param:
                        self._rate_unit = rateUnit
                        rateUnitIsOK = True
                        break
                if not rateUnitIsOK:
                   raise ValueError('Rate unit unknown')

    # -------------------------------------------------------------------------
    # volume
    @property
    def volume(self):
        return self._get_return_data([self._volume, self._volume_unit])

    @volume.setter
    def volume(self, new_volume):
        if type(new_volume) == float:
            if not (self._min_volume <= new_volume <= self._max_volume):
               raise ValueError('Volume out of range [%f, %f]' % (self._min_volume, self._max_volume))
            # set volume (float)
            self._volume = new_volume
        elif type(new_volume) == str:
            volUnitIsOK = False
            for volUnit in NE1002xVolumeUnits:
                if str(volUnit) in new_volume:
                    self._volume_unit = volUnit
                    volUnitIsOK = True
                    break
            if not volUnitIsOK:
               raise ValueError('Volume unit unknown')

    # -------------------------------------------------------------------------
    # diameter
    @property
    def diameter(self):
        return self._get_return_data([self._diameter])

    @diameter.setter
    def diameter(self, new_diameter):
        if not (self._min_inside_diameter <= new_diameter <= self._max_inside_diameter):
           raise ValueError('Diameter out of range [%f, %f]' % (self._min_inside_diameter, self._max_inside_diameter))
        # set diameter
        self._diameter = new_diameter

    # -------------------------------------------------------------------------
    # volume_dispensed
    @property
    def volume_dispensed(self):
        return self._get_return_data(["I%s" % self._volume_dis_infused, "W%s" % self._volume_dis_withdrawn, self._volume_dis_unit])


    # -------------------------------------------------------------------------
    # ACTIONS
    # -------------------------------------------------------------------------
    # clear_volume_infused
    def clear_volume_infused(self):
        self._volume_dis_infused = 0.0

    # clear_volume_withdrawn
    def clear_volume_withdrawn (self):
        self._volume_dis_withdrawn = 0.0

    # start_pumping
    def start_pumping(self):
        if self._direction == NE1002xDirections.withdraw:
            self._operation = NE1002xOperations.withdrawing
        elif self._direction == NE1002xDirections.infuse:
            self._operation = NE1002xOperations.infusing

    # start_purge
    def start_purge(self):
        self._operation = NE1002xOperations.purging

    # pause_pumping
    def pause_pumping(self):
        self._operation = NE1002xOperations.prog_paused

    # stop_pumping
    def stop_pumping(self):
        if self._operation in (NE1002xOperations.infusing, NE1002xOperations.withdrawing):
            self._operation = NE1002xOperations.prog_paused
        elif self._operation == NE1002xOperations.prog_paused:
            self._operation = NE1002xOperations.prog_stopped
        else:
            self._operation = NE1002xOperations.idle


class NE1002xStreamInterface(StreamInterface):
    """
    NE1002x TCP stream interface

    This is the interface of a simulated NE1002x device. The device listens on a configured
    host:port-combination, one option to connect to it is via telnet:

        $ telnet host port

    Once connected, it's possible to send the specified commands, described in the dynamically
    generated documentation. Information about host, port and line terminators in the concrete
    device instance are also generated dynamically.
    """

    commands = {
        Cmd('get_firmware', pattern='^VER$'),
        Cmd('get_direction', pattern='^DIR$'),
        Cmd('set_direction', pattern='^DIR([A-Z]*)$', argument_mappings=(str,)),
        Cmd('get_rate', pattern='^RAT$'),
        Cmd('set_rate', pattern='^RAT([0-9]*\.?[0-9]+)(UM|MM|UH|MH)$', argument_mappings=(float,str,)),
        Cmd('get_volume', pattern='^VOL$'),
        Cmd('set_volume', pattern='^VOL([0-9]*\.?[0-9]+)$', argument_mappings=(float,)),
        Cmd('set_volume', pattern='^VOL(UL|ML)$', argument_mappings=(str,)),
        Cmd('get_diameter', pattern='^DIA$'),
        Cmd('set_diameter', pattern='^DIA([0-9]*\.?[0-9]+)$', argument_mappings=(float,)),
        Cmd('get_volume_dispensed', pattern='^DIS$'),
        Cmd('clear_volume_withdrawn', pattern='^CLD WDR$'),
        Cmd('clear_volume_infused', pattern='^CLD INF$'),
        Cmd('start_pumping', pattern='^RUN$'),
        Cmd('start_purge', pattern='^PUR$'),
        Cmd('pause_pumping', pattern='^PAS$'),
        Cmd('stop_pumping', pattern='^STP$')
    }

    #in_terminator   = '\r'
    in_terminator   = chr(CR)           # terminator TO device (receiving a message)
    out_terminator  = chr(ETX)          # terminator FROM device (sending a message)

    def _success(self):
        # ---------------------------------------------------------------------
        # returning success with the format:
        #   <START OF TEXT terminator>,<DEVICE ADDRESS>,<DEVICE STATUS>
        # ---------------------------------------------------------------------
        return "%s%s%s" % (chr(STX), self.device._address, self.device.status)

    def _error(self, error):
        # ---------------------------------------------------------------------
        # Message/Error - enum [ ? | NA | OOR | COM | IGN ]
        #    >  ?   = Command is not recognized (‘?’ only)
        #    >  NA  = Command is not currently applicable
        #    >  OOR = Command data is out of range
        #    >  COM = Invalid communications packet received
        #    >  IGN = Command ignored due to a simultaneous new Phase start
        # ---------------------------------------------------------------------
        # returning success with the format:
        #   <START OF TEXT terminator>,<DEVICE ADDRESS>,<DEVICE STATUS>,<ERROR>
        # ---------------------------------------------------------------------
        return "%s%s%s%s" % (chr(STX), self.device._address, self.device.status, error)

    # -------------------------------------------------------------------------
    # PROPERTIES
    # -------------------------------------------------------------------------
    # query firmware version
    def get_firmware(self):
        return self.device.firmware_version

    # -------------------------------------------------------------------------
    # direction
    def get_direction(self):
        return self.device.direction

    def set_direction(self, new_direction):
        try:
            self.device.direction = new_direction
        except RuntimeError:
            return self._error('NA')
        except ValueError:
            return self._error('OOR')
        except:
            print("set_direction - error!")

        return self._success()

    # -------------------------------------------------------------------------
    # rate
    def get_rate(self):
        return self.device.rate

    def set_rate(self, new_rate, new_rate_unit):
        try:
            self.device.rate = (new_rate, new_rate_unit)
        except RuntimeError:
            return self._error('NA')
        except ValueError:
            return self._error('OOR')
        except:
            print("set_rate - error!")

        return self._success()

    # -------------------------------------------------------------------------
    # volume
    def get_volume(self):
        return self.device.volume

    def set_volume(self, new_volume):
        try:
            self.device.volume = new_volume
        except RuntimeError:
            return self._error('NA')
        except ValueError:
            return self._error('OOR')
        except:
            print("set_volume - error!")

        return self._success()

    # -------------------------------------------------------------------------
    # diameter
    def get_diameter(self):
        return self.device.diameter

    def set_diameter(self, new_diameter):
        try:
            self.device.diameter = new_diameter
        except RuntimeError:
            return self._error('NA')
        except ValueError:
            return self._error('OOR')
        except:
            print("set_diameter - error!")

        return self._success()

    # -------------------------------------------------------------------------
    # volume_dispensed
    def get_volume_dispensed(self):
        return self.device.volume_dispensed

    # -------------------------------------------------------------------------
    # ACTIONS
    # -------------------------------------------------------------------------
    # clear_volume_withdrawn
    def clear_volume_withdrawn(self):
        try:
            self.device.clear_volume_withdrawn()
        except RuntimeError:
            return self._error('NA')
        except:
            print("clear_volume_withdrawn - error!")

        return self._success()

    # -------------------------------------------------------------------------
    # clear_volume_infused
    def clear_volume_infused(self):
        try:
            self.device.clear_volume_infused()
        except RuntimeError:
            return self._error('NA')
        except:
            print("clear_volume_infused - error!")

        return self._success()

    # -------------------------------------------------------------------------
    # start_pumping
    def start_pumping(self):
        try:
            self.device.start_pumping()
        except RuntimeError:
            return self._error('NA')
        except:
            print("start_pumping - error!")

        return self._success()

    # -------------------------------------------------------------------------
    # start_purge
    def start_purge(self):
        try:
            self.device.start_purge()
        except RuntimeError:
            return self._error('NA')
        except:
            print("start_purge - error!")

        return self._success()

    # -------------------------------------------------------------------------
    # pause_pumping
    def pause_pumping(self):
        try:
            self.device.pause_pumping()
        except RuntimeError:
            return self._error('NA')
        except:
            print("pause_pumping - error!")

        return self._success()

    # -------------------------------------------------------------------------
    # stop_pumping
    def stop_pumping(self):
        try:
            self.device.stop_pumping()
        except RuntimeError:
            return self._error('NA')
        except:
            print("stop_pumping - error!")

        return self._success()

# -----------------------------------------------------------------------------
# LeWIS framework version associated
# -----------------------------------------------------------------------------
framework_version = '1.2.2'

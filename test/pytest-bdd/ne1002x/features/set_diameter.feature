Feature: Set syringe inner diameter in mm

    Scenario Outline: Input diameters
      Given user enters <diameter> value
      When user presses Enter key
      Then entered value should <set_result> set into device

      Examples:
      | diameter | set_result |
      | -1.5 mm  | not be     |
      |  0.0 mm  | not be     |
      |  0.1 mm  | be         |
      |  2.0 mm  | be         |
      | 17.2 mm  | be         |
      | 50.0 mm  | be         |
      | 50.1 mm  | not be     |
      | 99.0 mm  | not be     |
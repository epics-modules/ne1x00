from pytest_bdd import scenario, given, when, then

@scenario(
    'set_diameter.feature',
    'Input diameters',
    example_converters=dict(diameter=str, set_result=str)
)
def test_outlined():
    pass

@given('user enters <diameter> value')
def step_given_entered_value(diameter):
    assert isinstance(diameter, str)
    return dict(diameter=diameter)

@when('user presses Enter key')
def step_when_press_enter(step_given_entered_value):
    assert True is not False

@then('entered value should <set_result> set into device')
def step_then_verify_set_result(step_given_entered_value, set_result):
    assert isinstance(set_result, str)
    step_given_entered_value['set_result'] = set_result
    assert True is not False

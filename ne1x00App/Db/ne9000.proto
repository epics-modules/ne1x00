# -----------------------------------------------------------------------------
# EPICS - Protocol used by StreamDevice module
# -----------------------------------------------------------------------------
# Syringe Pump - NE 9000;
# -----------------------------------------------------------------------------
# ESS ERIC - ICS HWI group, 2019-2020
# -----------------------------------------------------------------------------
# WP12 - tamas.kerenyi@ess.eu
# WP12 - douglas.bezerra.beniz@ess.eu
# -----------------------------------------------------------------------------

InTerminator  = ETX;
OutTerminator = CR;
#ReplyTimeout = 1000;

address = "%*d";
# -----------------------------------------------------------------------------
# Status - enum [ I | W | S | P | T | U | X ]
#    >  I = Infusing
#    >  W = Withdrawing
#    >  S = Pumping Program Stopped
#    >  P = Pumping Program Paused
#    >  T = Pause Phase
#    >  U = Operational trigger wait (user wait)
#    >  X = Purging
# -----------------------------------------------------------------------------
status  = "%(\$1)/[IWSPTUX]|A?\w/";

# <start of text>,<pump address>,<pump status>
prompt = STX,${address},${status};


# -----------------------------------------------------------------------------
# Get pumping DIRECTION
#  - dir: enum [ INF | WDR | REV ]
# -----------------------------------------------------------------------------
direction = "%{INF|WDR|REV}";
get_direction {
    out "DIR";
    in ${prompt},${direction};
}

# -----------------------------------------------------------------------------
# Set pumping DIRECTION
#  - dir: enum [ INF | WDR | REV ]
#    >  INF = Dispense
#    >  WDR = Withdraw
#    >  REV = Reverse pumping direction
# -----------------------------------------------------------------------------
set_direction {
    direction = "%{INF|WDR|REV}";
    out "DIR",${direction};
    in ${prompt};
    @mismatch { in "%*/00[IWSPTUX]/%*/00[IWSPTUX]/%(\$2)s" };
    @init { get_direction; }
}

# -----------------------------------------------------------------------------
# Get syringe DIAMETER
#  - syntax: DIA [ nn / nn ]
# -----------------------------------------------------------------------------
syringe_diameter = "%s";
get_diameter {
    out "DIA";
    in ${prompt},${syringe_diameter};
}

# -----------------------------------------------------------------------------
# Set syringe DIAMETER
#  - syntax: DIA [ nn / nn ]
#  - limits: tubing inside diameter should be smaller than 1" (inch)
# -----------------------------------------------------------------------------
set_diameter {
    format = "%(\$2)2d\/%(\$3)2d";
    out "DIA",${format};
    in ${prompt};
    @mismatch { in "%*/00[IWSPTUX]/%(\$4)s" };
    @init { get_diameter; }
}


# -----------------------------------------------------------------------------
# Calibrates the tubing diameter using <float> as the measured dispense volume,
#   and the volume dispensed or withdrawn, according to the current pumping
#   direction. The calibration value cannot be queried. The pump must be stopped
#   to calibrate.
# ---
#  - syntax: CAL <float>
#  - max 4 digits + 1 decimal point send;
# -----------------------------------------------------------------------------
calib_3decimal_points {
    format = "%.3f";
    out "CAL",${format};
    in ${prompt};
    @mismatch { in "%*/00[IWSPTUX]/%(\$2)s" };
}

calib_2decimal_points {
    format = "%.2f";
    out "CAL",${format};
    in ${prompt};
    @mismatch { in "%*/00[IWSPTUX]/%(\$2)s" };
}

calib_1decimal_point {
    format = "%.1f";
    out "CAL",${format};
    in ${prompt};
    @mismatch { in "%*/00[IWSPTUX]/%(\$2)s" };
}

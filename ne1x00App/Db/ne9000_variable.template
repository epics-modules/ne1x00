# -----------------------------------------------------------------------------
# EPICS - Database
# -----------------------------------------------------------------------------
# Syringe Pump - NE 9000;
# -----------------------------------------------------------------------------
# ESS ERIC - ICS HWI group, 2019-2020
# -----------------------------------------------------------------------------
# WP12 - tamas.kerenyi@ess.eu
# WP12 - douglas.bezerra.beniz@ess.eu
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
# Get syringe DIAMETER
# -----------------------------------------------------------------------------
record(stringin, $(P):DIAMETER) {
    field(DESC, "Inside diameter of the syringe")
    field(DTYP, "stream")
    field(INP,  "@ne9000.proto get_diameter($(P):STATUS) $(PORT) $(ADDR)")
    field(SCAN, "Passive")
    field(PINI, "YES")
    field(FLNK, "$(P):VOLUME_INFUSED")
}


# -----------------------------------------------------------------------------
# Set syringe DIAMETER
#  - syntax: DIA [ nn / nn ]
#  - limits: tubing inside diameter should be smaller than 1" (inch)
# -----------------------------------------------------------------------------
record(ao, $(P):SET_DIAMETER) {
    field(DESC, "Set inside diameter")
    field(DTYP, "stream")
    field(OUT,  "@ne9000.proto set_diameter($(P):STATUS,$(P):SET_DIAMETER_NUM,$(P):SET_DIAMETER_DEN,$(P):MESSAGE) $(PORT) $(ADDR)")
}

record(ao, $(P):SET_DIAMETER_NUM) {
    field(DESC, "Numerator input for diameter")
    field(DTYP, "Soft Channel")
    field(FLNK, "$(P):DIA_SET__")
}

record(ao, $(P):SET_DIAMETER_DEN) {
    field(DESC, "Denominator input for diameter")
    field(DTYP, "Soft Channel")
    field(FLNK, "$(P):DIA_SET__")
}

record(fanout, $(P):DIA_SET__) {
    field(DESC, "(int.) Diameter set")
    field(LNK1, "$(P):MSG_CLEAR__")
    field(LNK2, "$(P):SET_DIAMETER")
}

# -----------------------------------------------------------------------------
# Get pumping RATE value and unit
# -----------------------------------------------------------------------------
record(ai, $(P):RATE) {
    field(DESC, "Pumping rate")
    field(DTYP, "stream")
    field(INP,  "@ne1x00.proto get_rate($(P):STATUS,$(P):RATE_UNITS,$(RATE_UNITS_ENUM)) $(PORT) $(ADDR)")
    field(SCAN, "$(SCAN) second")
    field(PINI, "YES")
    field(FLNK, "$(P):DIRECTION")
}

record(mbbi, $(P):RATE_UNITS) {
    field(DESC, "Pumping rate units")
    field(DTYP, "Soft Channel")
    field(ZRVL, "0")
    field(ZRST, "$(RATE_UNIT_1ST)")
    field(ONVL, "1")
    field(ONST, "$(RATE_UNIT_2ND)")
    field(TWVL, "2")
    field(TWST, "$(RATE_UNIT_3RD)")
    field(THVL, "3")
    field(THST, "$(RATE_UNIT_4TH)")
    field(SCAN, "Passive")
}

record(ao, $(P):RATE_SET_3D__) {
    field(DESC, "(int.) Set rate 3 digits")
    field(DTYP, "stream")
    field(DOL,  "$(P):SET_RATE CP")
    field(OMSL, "closed_loop")
    field(OUT,  "@ne1x00.proto set_rate_3decimal_points($(P):STATUS,$(P):SET_RATE_UNITS,$(RATE_UNITS_ENUM),$(P):MESSAGE) $(PORT) $(ADDR)")
    field(FLNK, "$(P):RATE_CHK_3D__")
}

record(ao, $(P):RATE_SET_2D__) {
    field(DESC, "(int.) Set rate 2 digits")
    field(DTYP, "stream")
    field(DOL,  "$(P):SET_RATE")
    field(OMSL, "closed_loop")
    field(OUT,  "@ne1x00.proto set_rate_2decimal_points($(P):STATUS,$(P):SET_RATE_UNITS,$(RATE_UNITS_ENUM),$(P):MESSAGE) $(PORT) $(ADDR)")
    field(FLNK, "$(P):RATE_CHK_2D__")
}

record(ao, $(P):RATE_SET_1D__) {
    field(DESC, "(int.) Set rate 1 digit")
    field(DTYP, "stream")
    field(DOL,  "$(P):SET_RATE")
    field(OMSL, "closed_loop")
    field(OUT,  "@ne1x00.proto set_rate_1decimal_point($(P):STATUS,$(P):SET_RATE_UNITS,$(RATE_UNITS_ENUM),$(P):MESSAGE) $(PORT) $(ADDR)")
    field(FLNK, "$(P):RATE_CHK_ALARM__")
}

# -----------------------------------------------------------------------------
# Set pumping RATE value and unit
#  - val:   max 4 digits + 1 decimal point send;
#  - units: enum [ UM | MM | UH | MH ]
#    >  UM = μL / min
#    >  MM = mL / min
#    >  UH = μL / hr
#    >  MH = mL / hr
# -----------------------------------------------------------------------------
record(mbbo, $(P):SET_RATE_UNITS) {
    field(DESC, "Desired pumping rate")
    field(ZRVL, "0")
    field(ZRST, "$(RATE_UNIT_1ST)")
    field(ONVL, "1")
    field(ONST, "$(RATE_UNIT_2ND)")
    field(TWVL, "2")
    field(TWST, "$(RATE_UNIT_3RD)")
    field(THVL, "3")
    field(THST, "$(RATE_UNIT_4TH)")
    field(UNSV, "MAJOR")
    field(FLNK, "$(P):RATE_UNITS_RST__")
}


# -----------------------------------------------------------------------------
# Get syringe VOLUME value and unit
# -----------------------------------------------------------------------------
record(ai, $(P):VOLUME) {
    field(DESC, "Volume to be pumped")
    field(DTYP, "stream")
    field(INP,  "@ne1x00.proto get_volume($(P):STATUS,$(P):VOLUME_UNITS,$(VOL_UNITS_ENUM)) $(PORT) $(ADDR)")
    field(SCAN, "Passive")
    field(PINI, "YES")
    field(FLNK, "$(P):DIAMETER")
}

record(mbbi, $(P):VOLUME_UNITS) {
    field(DESC, "Volume units")
    field(DTYP, "Soft Channel")
    field(ZRVL, "0")
    field(ZRST, "$(VOL_UNIT_1ST)")
    field(ONVL, "1")
    field(ONST, "$(VOL_UNIT_2ND)")
    field(SCAN, "Passive")
}

record(ao, $(P):VOL_SET_3D__) {
    field(DESC, "(int.) Set vol. 3 digits")
    field(DTYP, "stream")
    field(DOL,  "$(P):SET_VOLUME CP")
    field(OMSL, "closed_loop")
    field(OUT,  "@ne1x00.proto set_volume_3decimal_points($(P):STATUS,$(P):SET_VOLUME_UNITS,$(VOL_UNITS_ENUM),$(P):MESSAGE) $(PORT) $(ADDR)")
    field(FLNK, "$(P):VOL_CHK_3D__")
}

record(ao, $(P):VOL_SET_2D__) {
    field(DESC, "(int.) Set vol. 2 digits")
    field(DTYP, "stream")
    field(DOL,  "$(P):SET_VOLUME")
    field(OMSL, "closed_loop")
    field(OUT,  "@ne1x00.proto set_volume_2decimal_points($(P):STATUS,$(P):SET_VOLUME_UNITS,$(VOL_UNITS_ENUM),$(P):MESSAGE) $(PORT) $(ADDR)")
    field(FLNK, "$(P):VOL_CHK_2D__")
}

record(ao, $(P):VOL_SET_1D__) {
    field(DESC, "(int.) Set vol. 1 digit")
    field(DTYP, "stream")
    field(DOL,  "$(P):SET_VOLUME")
    field(OMSL, "closed_loop")
    field(OUT,  "@ne1x00.proto set_volume_1decimal_point($(P):STATUS,$(P):SET_VOLUME_UNITS,$(VOL_UNITS_ENUM),$(P):MESSAGE) $(PORT) $(ADDR)")
    field(FLNK, "$(P):VOL_CHK_ALARM__")
}

# -----------------------------------------------------------------------------
# Set pumping VOLUME unit
#  - units: enum [ UL | ML ]
#    >  UL = μL (microliters)
#    >  ML = mL (milliliters)
# -----------------------------------------------------------------------------
record(mbbo, $(P):SET_VOLUME_UNITS) {
    field(DESC, "Pumping volume unit")
    field(ZRVL, "0")
    field(ZRST, "$(VOL_UNIT_1ST)")
    field(ONVL, "1")
    field(ONST, "$(VOL_UNIT_2ND)")
    field(UNSV, "MAJOR")
    field(FLNK, "$(P):VOL_UNITS_RST__")
}


# -----------------------------------------------------------------------------
# Get volume infused and withdrawn
# -----------------------------------------------------------------------------
record(ai, $(P):VOLUME_INFUSED) {
    field(DESC, "Volume infused so far")
    field(DTYP, "stream")
    field(INP,  "@ne1x00.proto get_volume_dispensed($(P):STATUS,$(P):VOLUME_WITHDRAWN,$(P):VOLUME_DIS_UNIT,$(VOL_UNITS_ENUM)) $(PORT) $(ADDR)")
    field(SCAN, "Passive")
    field(PINI, "YES")
}

record(ai, $(P):VOLUME_WITHDRAWN) {
    field(DESC, "Volume withdrawn so far")
}

record(mbbi, $(P):VOLUME_DIS_UNIT) {
    field(DESC, "Unit for WDR and INF volumes")
    field(DTYP, "Soft Channel")
    field(ZRVL, "0")
    field(ZRST, "$(VOL_UNIT_1ST)")
    field(ONVL, "1")
    field(ONST, "$(VOL_UNIT_2ND)")
    field(SCAN, "Passive")
}


# -----------------------------------------------------------------------------
# Get/Set pumping DIRECTION
#  - dir: enum [ INF | WDR | REV | STK ]
# -----------------------------------------------------------------------------
record(mbbi, $(P):DIRECTION) {
    field(DESC, "Pumping direction")
    field(DTYP, "stream")
    field(ZRVL, "0")
    field(ZRST, "Dispense")
    field(ONVL, "1")
    field(ONST, "Withdraw")
    field(TWVL, "2")
    field(TWST, "Reverse pumping")
    field(INP,  "@ne9000.proto get_direction($(P):STATUS) $(PORT) $(ADDR)")
    field(SCAN, "Passive")
    field(PINI, "YES")
    field(FLNK, "$(P):VOLUME")
}

record(mbbo, $(P):SET_DIRECTION) {
    field(DESC, "Desired pumping direction")
    field(DTYP, "stream")
    field(ZRVL, "0")
    field(ZRST, "Dispense")
    field(ONVL, "1")
    field(ONST, "Withdraw")
    field(TWVL, "2")
    field(TWST, "Reverse pumping")
    field(UNSV, "MAJOR")
    field(OUT,  "@ne9000.proto set_direction($(P):STATUS,$(P):MESSAGE) $(PORT) $(ADDR)")
    field(FLNK, "$(P):DIRECTION")
}


# -----------------------------------------------------------------------------
# Calibrates the tubing diameter using <float> as the measured dispense volume,
#   and the volume dispensed or withdrawn, according to the current pumping
#   direction. The calibration value cannot be queried. The pump must be stopped
#   to calibrate.
# ---
#  - syntax: CAL <float>
#  - max 4 digits + 1 decimal point send;
# -----------------------------------------------------------------------------
record(bo, $(P):CALIB) {
    field(DESC, "Start calibration")
    field(DTYP, "Soft Channel")
    field(FLNK, "$(P):CAL_RST__")
}

record(ao, $(P):SET_CALIB) {
    field(DESC, "Input for calibration volume")
    field(DTYP, "Soft Channel")
    field(FLNK, "$(P):MSG_CLEAR__")
}

record(fanout, $(P):CAL_RST__) {
    field(DESC, "(int.) Calibrate reset")
    field(LNK1, "$(P):MSG_CLEAR__")
    field(LNK2, "$(P):CAL_CLR_ALARM__")
    field(LNK3, "$(P):CAL_SET_3D__")
}

record(calcout, $(P):CAL_CHK_ALARM__) {
    field(DESC, "(int.) Calibrate alarm")
    field(INPA, "$(P):CAL_SET_3D__.SEVR")
    field(INPB, "$(P):CAL_SET_2D__.SEVR")
    field(INPC, "$(P):CAL_SET_1D__.SEVR")
    field(CALC, "((A=3)&&(B=3)&&(C=3))")
    field(OOPT, "When Non-zero")
    field(OUT,  "$(P):CAL_SET_ALARM__.PROC")
}

record(ao, $(P):CAL_SET_ALARM__) {
    field(DESC, "(int.) Calib. alarm set")
    field(VAL, "3")
    field(OUT, "$(P):SET_CALIB.HHSV")
}

record(ao, $(P):CAL_CLR_ALARM__) {
    field(DESC, "(int.) Calib. alarm clear")
    field(VAL, "0")
    field(OUT, "$(P):SET_CALIB.HHSV")
}

record(ao, $(P):CAL_SET_3D__) {
    field(DESC, "(int.) Set calib. 3 digits")
    field(DTYP, "stream")
    field(DOL,  "$(P):SET_CALIB")
    field(OMSL, "closed_loop")
    field(OUT,  "@ne9000.proto calib_3decimal_points($(P):STATUS,$(P):MESSAGE) $(PORT) $(ADDR)")
    field(FLNK, "$(P):CAL_CHK_3D__")
}

record(calcout, $(P):CAL_CHK_3D__) {
    field(DESC, "(int.) Set calib. 3 digits")
    field(INPA, "$(P):CAL_SET_3D__.SEVR")
    field(CALC, "A#3")
    field(OOPT, "When Zero")
    field(OUT,  "$(P):CAL_SET_2D__.PROC")
    field(FLNK, "$(P):MSG_CLEAR__")
}

record(ao, $(P):CAL_SET_2D__) {
    field(DESC, "(int.) Set calib. 2 digits")
    field(DTYP, "stream")
    field(DOL,  "$(P):SET_CALIB")
    field(OMSL, "closed_loop")
    field(OUT,  "@ne9000.proto calib_2decimal_points($(P):STATUS,$(P):MESSAGE) $(PORT) $(ADDR)")
    field(FLNK, "$(P):CAL_CHK_2D__")
}

record(calcout, $(P):CAL_CHK_2D__) {
    field(DESC, "(int.) Set calib. 2 digits")
    field(INPA, "$(P):CAL_SET_2D__.SEVR")
    field(CALC, "A#3")
    field(OOPT, "When Zero")
    field(OUT,  "$(P):CAL_SET_1D__.PROC")
    field(FLNK, "$(P):MSG_CLEAR__")
}

record(ao, $(P):CAL_SET_1D__) {
    field(DESC, "(int.) Set calib. 1 digit")
    field(DTYP, "stream")
    field(DOL,  "$(P):SET_CALIB")
    field(OMSL, "closed_loop")
    field(OUT,  "@ne9000.proto calib_1decimal_point($(P):STATUS,$(P):MESSAGE) $(PORT) $(ADDR)")
    field(FLNK, "$(P):CAL_CHK_ALARM__")
}

# -----------------------------------------------------------------------------
# NE 1600 syringe pump startup cmd
# -----------------------------------------------------------------------------
require(stream, 2.8.8)
require(ne1x00, 0.0.1)
require(iocStats, 3.1.16)

# -----------------------------------------------------------------------------
# setting parameters when not using auto deployment
# -----------------------------------------------------------------------------
epicsEnvSet(PORTNAME, "PortA")
epicsEnvSet(IPADDR, "172.30.244.76")
epicsEnvSet(IPPORT, "4001")
epicsEnvSet(LOCATION, "Utgard; $(IPADDR)")
epicsEnvSet(PREFIX, "UTG-SEE-PREMP:NE1600-001")
epicsEnvSet(IOCNAME, "$(PREFIX)")
epicsEnvSet(SCAN, "1 second")
epicsEnvSet(STREAM_PROTOCOL_PATH, "$(ne1x00_DIR)db")

# -----------------------------------------------------------------------------
# loading databases
# -----------------------------------------------------------------------------
# Statistics database
iocshLoad("$(iocStats_DIR)iocStats.iocsh", "IOCNAME=$(IOCNAME)")

# NE 1600 syringe pump
iocshLoad("$(ne1x00_DIR)ne1600.iocsh", "PREFIX=$(PREFIX), IPADDR=$(IPADDR), IPPORT=$(IPPORT), SCAN=$(SCAN)")

# ne1x00

European Spallation Source ERIC Site-specific EPICS module: ne1x00

Additonal information:
* [Documentation](https://confluence.esss.lu.se/display/IS/NE1002x+Syringe+Pump)
* [Release notes](RELEASE.md)
* [Requirements](https://gitlab.esss.lu.se/nice/staging-recipes/ne1x00-recipe/-/blob/master/recipe/meta.yaml#L18)
* [Building and Testing](https://confluence.esss.lu.se/display/IS/1.+Development+locally+with+e3+and+Conda#id-1.Developmentlocallywithe3andConda-BuildanEPICSmoduleandtestitlocally)
